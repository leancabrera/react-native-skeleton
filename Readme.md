La idea es tener separada la lógica de la parte de representación.
Esto permite poder testear por separado tanto la logica, las screens y los componentes.
Para eso se crean 3 carpetas que mantienen consistente esa separación y al mismo tiempo permiten una gran reutilizacion, testeo y escalabilidad.

## Dependencias

el proyecto no necesita grandes dependencias ya que por su logica de separación se puede mantener simple (clean code).
Ante la pregunta de por que no usar Redux/Mobx, la respuesta es que no es necesario. Dado que la logica esta separada de la UI, la informacion pasa de una lado a otro completamente transparente. En caso de necesitar que dos componentes "child" necesiten compartir informacion se podria usar Context API de React (KISS principle).

Para el testing usaria jest + enzyme. Permite testear tanto la parte logica como la UI.

Se incluye _prop-types_ obligatoriamente para validar las propiedades de los componentes

Seria necesario incluir _eslint_ para validar el código.

Habria que usar _husky_ para evitar commits con errores

### /components

- componentes independientes.
- elementos de la UI

### /containers

- son componentes encargados de la parte lógica. Ej. obtener la data de la API

### /screens

- son componentes que reciben la data desde los containers y la implementan reutilizando _components_

### /services

- en esta carpeta se separa la lógica de comunicación con la API

### /config

- se usan diferentes configs por enviroments
