import React, { Component } from "react";
import FirstContainer from "./src/containers/FirstContainer";
import api from "./src/services/api";
import config from "./src/config";

api.setConfig({ ...config });

export default class App extends Component {
  render() {
    return <FirstContainer />;
  }
}
