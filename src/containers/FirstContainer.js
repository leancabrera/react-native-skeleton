import React from "react";
import api from "../services/api";
import FirstScreen from "../screens/FirstScreen";

const propTypes = {};
const defaultProps = {};

class containerName extends React.Component {
  static propTypes = propTypes;
  static defaultProps = defaultProps;
  state = {
    loading: true,
    data: []
  };

  componentWillMount() {
    this.fetchData();
  }

  async fetchData() {
    const response = await api.getUsers();

    this.setState({ loading: false, data: response });
  }

  render() {
    return <FirstScreen data={this.state.data} loading={this.state.loading} />;
  }
}
export default containerName;
