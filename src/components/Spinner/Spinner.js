import React from "react";
import { ActivityIndicator, View } from "react-native";

const styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    flexDirection: "row"
  }
};

const Spinner = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" />
  </View>
);

export default Spinner;
