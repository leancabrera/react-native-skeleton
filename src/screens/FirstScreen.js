import React from "react";
import { View, Text } from "react-native";
import { bool, array } from "prop-types";

import Spinner from "../components/Spinner/Spinner";

const propTypes = {
  loading: bool.isRequired,
  data: array.isRequired
};
const defaultProps = {};

const renderUsers = data => {
  return data.map(e => <Text key={e.id}>{e.name}</Text>);
};

const FirstScreen = props => {
  return props.loading ? <Spinner /> : <View>{renderUsers(props.data)}</View>;
};

FirstScreen.propTypes = propTypes;
FirstScreen.defaultProps = defaultProps;
export default FirstScreen;
