class Api {
  constructor() {
    this.config = {};
  }

  async request({ url, data, method }) {
    return new Promise((resolve, reject) => {
      fetch(`${this.config.api.baseUrl}${url}`, {
        method: method || "get",
        body: data ? JSON.stringify(data) : null,
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        }
      })
        .then(response => {
          response
            .json()
            .then(json => {
              console.log(json);

              resolve(json);
            })
            .catch(err => {
              resolve(err);
            });
        })
        .catch(error => {
          reject(Object.assign({}, error, { status: error.status }));
        });
    }).catch(error => error);
  }

  setConfig(config) {
    console.log(config);
    this.config = config;
    return this;
  }

  getUsers() {
    return this.request({
      url: `/users`
    });
  }
}

const api = new Api();
export default api;
